package org.sample;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.log;

public class Main {
    public static Vector[] vectors;
    public static Vector resultMultiThreads;

    public static int COUNT_OF_STEPS = 4;
    public static int COUNT_OF_NUMBER = 4;
    public static int LOG_COT = (int) (log(COUNT_OF_NUMBER) / log(2));
    public static int SECTION = COUNT_OF_STEPS / COUNT_OF_NUMBER;

    public static Wall wall = null;
    static Vector[][] vectorTable;

    public static void main(String[] args) throws InterruptedException {
        int steps[][] = {{45, 40}, {30, 50}, {105, 40}, {90, 20}};

        vectors = new Vector[COUNT_OF_STEPS];
        for (int i = 0; i < COUNT_OF_STEPS; i++) {
            vectors[i] = new Vector(steps[i][1] * cos(steps[i][0]), steps[i][1] * sin(steps[i][0]), steps[i][0]);
        }

        vectorTable = new Vector[LOG_COT + 1][COUNT_OF_NUMBER];

        for (int i = LOG_COT; i >= 0; i--) {
            for (int j = 0; j < COUNT_OF_NUMBER; j++) {
                vectorTable[i][j] = new Vector(0,0, 0);
            }
        }

        resultMultiThreads = new Vector(0, 0,0);

        wall = new Wall(COUNT_OF_NUMBER);

        for (int i = COUNT_OF_NUMBER - 1; i > 0; i--) {
            new PrefixScan(i, vectorTable).start();
        }

        PrefixScan last = new PrefixScan(0, vectorTable);
        last.start();
        last.join();

        resultMultiThreads = vectorTable[0][0];
        Vector resultSingleThread;
        resultSingleThread = singleThreadImpl(steps);
        System.out.println("Result multithreads " + resultMultiThreads.toString());
        System.out.println("Result singlethread " + resultSingleThread.toString());

    }

    public static Vector singleThreadImpl(int[][] steps) {
        Vector result = new Vector(0,0,0);
        vectors = new Vector[COUNT_OF_STEPS];
        for (int i = 0; i < COUNT_OF_STEPS; i++) {
           vectors[i] = new Vector(steps[i][1] * cos(steps[i][0]), steps[i][1] * sin(steps[i][0]), steps[i][0]);
        }
        for (int i = 0; i < COUNT_OF_STEPS; i++) {
            result = result.add(vectors[i]);
        }
        return result;
    }
}