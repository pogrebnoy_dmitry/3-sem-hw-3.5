package org.sample;

public class PrefixScan extends Thread {
    int idThread;
    Vector[][] vectorTable;

    PrefixScan(int idThread, Vector[][] vectorTable) {
        this.idThread = idThread;
        this.vectorTable = vectorTable;
    }

    public void run() {
        int depth = Main.LOG_COT;

        Vector vector = Main.vectors[idThread * Main.SECTION];
        for (int i = idThread * Main.SECTION + 1; i < (idThread + 1) * Main.SECTION; i++) {
            vector = vector.add(Main.vectors[i]);
        }
        vectorTable[depth][idThread] = vector;
        Main.wall.allSynchronize();
        prefix(depth);
        Main.wall.allSynchronize();
    }

    void prefix(int depth) {
        if (depth == 0) {
            Main.wall.allSynchronize();
            return;
        }

        if (idThread < 1 << (depth - 1)) {
            vectorTable[depth - 1][idThread] = vectorTable[depth][2 * idThread].add(vectorTable[depth][2 * idThread + 1]);
        }
        Main.wall.allSynchronize();
        prefix(depth - 1);
        Main.wall.allSynchronize();
    }

}