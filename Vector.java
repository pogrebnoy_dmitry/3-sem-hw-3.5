package org.sample;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Vector {
    double x;
    double y;
    int degree;

    public Vector(double x, double y, int degree) {
        this.x = x;
        this.y = y;
        this.degree = degree;
    }
    public String toString() {
        return ("(" + x + "," + y + "," + degree + ")");
    }

    public Vector turn(int degree) {
        return new Vector(x * cos(degree) - y * sin(degree), x * sin(degree) + y * cos(degree), this.degree);
    }

    public Vector add(Vector vector) {
        Vector v = vector.turn(degree);
        return (new Vector(this.x + v.x, this.y + v.y, this.degree + v.degree));
    }
}